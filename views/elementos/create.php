<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Elementos */

$this->title = 'Create Elementos';
$this->params['breadcrumbs'][] = ['label' => 'Elementos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="elementos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
