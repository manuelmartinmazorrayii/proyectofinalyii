<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

//LISTVIEW DEL CONTRINCANTE

$this->title = "Personajes";
?>

<style>    
.wrappercontrincante
{
        outline-style: solid;
        outline-width: 4px;
        outline-color: #0A141B;
        background-color:#0F1F2A;
        width: 500px;
        height: 220px;
        position: relative;
        left: 300px;
}
.leftpane {
    width: 25%;
    height: 100%;
    float: left;
    border-collapse: collapse;
}

.middlepane {
    width: 60%;
    float: right;
}

.textcolor
{
  color:#fa9632;
}

.textcolor em
{
  color:#fff3b8;
}

.imgleft
{
    border-right: 4px solid;
    border-color:  #0A141B;
}
</style>

<div class="wrappercontrincante">
    <div class="leftpane">
        <img class="imgleft" alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$enemyimgpath?>" width="200" height="220">
    </div>
    
    <div class="middlepane">
        <h3><?= $model->nombre ?></h3>
        <h4 class="textcolor">Nivel: <em><?= $model->nivel ?></em></h4> 
        <h4 class="textcolor">Clase: <em><?= $model->clase ?></em></h4>
        <h4 class="textcolor">
            Tipo:
            <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$enemyimgpathClass?>" width="25" height="25"> 
            Elemento:
            <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$enemyimgpathElement?>" width="20" height="20"> 
        </h4>
        <h4 class="textcolor"">Zona Favorita:</h4>
        <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.'/imgs/others/unknown.png'?>" width="25" height="25"> 
    </div>

</div>