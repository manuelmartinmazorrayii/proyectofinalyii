<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

//VIEW DE UN PERSONAJE SINGULAR, PARA QUE NO SALGAN REPETIDOS, SE USA AL CREAR EL PERSONAJE

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\ListView;


$this->title = 'Crear Personaje';
$url = Yii::getAlias("@web") . '/imgs/others/';
?>

<style>
body {
    background:url(<?= $url ?>bg1.png);
    background-attachment: fixed;
    background-size: cover;
        font-family: 'teen', serif;
}
</style>

<div class="jumbotron" style="color:#fa9632">
    <h1>Personaje Creado!</h1>
    
    <div>
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_personajes',
            'layout' => "{pager}\n{items}",
        ]);
        ?>
    </div>
</div>