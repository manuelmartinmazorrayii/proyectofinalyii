<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\ListView;

use app\models\Clases;
use app\models\Zonas;
use app\models\Elementos;

use app\controllers\ClasesController;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//RESULTADO DEL COMBATE SI MUERES! PARA QUE NO PUEDAS VOLVER A LUCHAR SI ESTAS MUERTO
$url = Yii::getAlias("@web") . '/imgs/others/';  
?>

<style>
body {
    background:url(<?= $url ?>bg1.png);
    background-attachment: fixed;
    background-size: cover;
        font-family: 'teen', serif;
}

.title {
  color:#fa9632;
  text-align: center;
}

.text {
  color:#fff3b8;
  text-align: center;
}

.imageborder
{
    border: 4px solid;
    border-color:  #0A141B;
  display: block;
  margin-left: auto;
  margin-right: auto;
}

.block
{
  display: block;
  margin-left: auto;
  margin-right: auto;
}
</style>

<h1 class="title"><?php echo $resultado ?></h1>
<img class="imageborder" src="<?php echo Yii::$app->request->baseUrl.$imgclase?>" width="200" height="200">
<h1 class="text"><?php echo $destinoniveles ?></h1>
<h1 class="text"><?php echo $destinojugador ?></h1>

<div style="display: flex; align-items: center; justify-content: center;">
    <p><?= Html::a('Volver', ['/personajes/personajes'], ['class'=>'btn btn-warning']) ?> </p>
</div>
