<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

//VIEW DE LAS ESTADISTICAS DE UN PERSONAJE

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\ListView;
use yii\grid\GridView;


$this->title = 'Crear Personaje';
$url = Yii::getAlias("@web") . '/imgs/others/';
?>

<style>    
  body {
    background:url(<?= $url ?>bg1.png);
    background-attachment: fixed;
    background-size: cover;
        font-family: 'teen', serif;
}  
.wrapper
{
        outline-style: solid;
        outline-width: 4px;
        outline-color: #0A141B;
        background-color:#0F1F2A;
        width: 900px;
        height: 400px;
        position: relative;
        left: 60px;
}
.leftpane {
    width: 20%;
    height: 51%;
    float: left;
    border-collapse: collapse;
    border-bottom: 4px solid;
    border-color:  #0A141B;
}
.leftpanebottom {
    width: 25%;
    height: 32.5%;
    float: left;
    border-collapse: collapse;
    border-bottom: 4px solid;
    border-right: 4px solid;
    border-color:  #0A141B;
}

.leftpanebottomextra {
    width: 25%;
    height: 17.5%;
    float: left;
    border-collapse: collapse;
        border-right: 4px solid;
    border-color:  #0A141B;
}

.rightpanebottom {
    width: 75%;
    height: 50%;
    float: right;
    border-collapse: collapse;
}

.middlepane {
    width: 50%;
    height: 51%;
    border-collapse: collapse;
    border-bottom: 4px solid;
    border-right: 4px solid;
    border-color:  #0A141B;
}

.rightpane {
  width: 50%;
  height: 51%;
  float: right;
  border-collapse: collapse;
    border-collapse: collapse;
    border-bottom: 4px solid;
    border-color:  #0A141B;

}

.textcolor
{
  color:#fa9632;
}

.textcolor em
{
  color:#fff3b8;
}

.textcolorG
{
  color: greenyellow;
}
.textcolorG em
{
  color:#fff3b8;
}

.textcolorR
{
  color: red;
}
.textcolorR em
{
  color:#fff3b8;
}

.imgleft
{
    border-right: 4px solid;
    border-color:  #0A141B;
}

.imgzone
{
outline-style: solid;
        outline-width: 4px;
        outline-color: #0A141B;
}
</style>
<div class="jumbotron" style="color:#fa9632">

<div class="wrapper">
    <div class="leftpane">
        <img class="imgleft" alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgpath?>" width="200" height="200">
    </div>
        
    <div class="rightpane">
         <h4 class="textcolor"  style="text-align: center;">
             Creado por: <em></em>
         </h4> 
        <h4 style="border-bottom: 4px solid; border-color: #0A141B;"></h4>
        
        <h4 class="textcolor">
            Fuerte Contra/En:
            <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgpathClassfuerte?>" width="25" height="25"> 
            <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgpathElementfuerte?>" width="20" height="20"> 
            <img class="imgzone" alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgpathZona?>" width="120" height="40"> 
        </h4>
        
        <h4 class="textcolor">
            &nbsp;&nbsp;Débil Contra/En:
            <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgpathClassdebil?>" width="25" height="25"> 
            <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgpathElementdebil?>" width="20" height="20"> 
            <img class="imgzone" alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgpathZonadebil?>" width="120" height="40"> 
        </h4>
    </div>
    
    <div class="middlepane">
        <h3></h3>
        <h4 class="textcolor">Nivel: <em><?= $mymodel->nivel?></em></h4> 
        <h4 class="textcolor">Clase: <em><?=  $mymodel->clase?></em></h4>
        <h4 class="textcolor">
            Tipo:
            <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgpathClass?>" width="25" height="25"> 
            Elemento:
            <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgpathElement?>" width="20" height="20"> 
        </h4>
        <h4 class="textcolor"">Zona Favorita:</h4>
        <img class="imgzone" alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgpathZona?>" width="120" height="40"> 
    </div>
    
    <div class="leftpanebottom">
        <h4 class="textcolorG">Victorias: <em> <?= $victories ?></em></h4> 
        <h4 class="textcolorR">Derrotas: <em> <?= $defeats ?></em></h4>
        <h4 class="textcolor">Matanzas: <em><?= $kills ?></em></h4>
        <h4 class="textcolor">Winrate: <em><?= $winrate ?>%</em></h4>
        <h4></h4>
    </div>
    
    <div class="rightpanebottom">
        <p class="textcolor">Historial de tus luchas</p>
        <div>
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_estadisticas',
                'viewParams' => ['imgpath' => $imgpath, 
                                 'imgpathClass' => $imgpathClass, 
                                 'imgpathElement' => $imgpathElement,
                                 'imgpathZona' => $imgpathZona],

                'layout' => "{pager}\n{items}",
            ]);
            ?>
        </div>
    </div>
    
    <div class="leftpanebottomextra">
        <h5 class="textcolor">Victima: <em><?= $victima?></em></h5> 
        <h5 class="textcolor">Némesis: <em><?= $nemesis?></em></h5>
    </div>
    
</div>
    <p></p>
        <?= Html::a('Volver', ['/personajes/personajes'], ['class'=>'btn btn-warning']) ?>
    

</div>

