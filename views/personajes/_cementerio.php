<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

//LISTVIEW DEL CEMENTERIO

$this->title = "Personajes";

//LAS IMAGENES QUE VAMOS A CARGAR
$imgpath = "";
$imgpathElement = "";
$imgpathClass = "";
$imgpathZone = "";

$imgpathElementDebil = "";
$imgpathElementFuerte = "";

$imgpathClassDebil = "";
$imgpathClassFuerte = "";

$imgpathZoneDebil = "";


//USANDO LA CLASE DEL MODELO, ELEGIR LAS IMAGENES APROPIADAS
switch ($model->clase)
    {
    //FUEGO
        case ('Herrero'):
            $imgpath = "/imgs/chars/Guerrero/Fuego.png";
            $imgpathElement = "/imgs/weapons/element/Fuego.png";
            $imgpathClass = "/imgs/weapons/class/Guerrero.png";
            $imgpathClassDebil = "/imgs/weapons/class/Mago.png";
            $imgpathClassFuerte = "/imgs/weapons/class/Arquero.png";
            $imgpathElementDebil = "/imgs/weapons/element/Agua.png";
            $imgpathElementFuerte = "/imgs/weapons/element/Tierra.png";
            break;
        case ('Caza-Dragones'):
            $imgpath = "/imgs/chars/Arquero/Fuego.jpg";
            $imgpathElement = "/imgs/weapons/element/Fuego.png";
            $imgpathClass = "/imgs/weapons/class/Arquero.png";
            $imgpathClassDebil = "/imgs/weapons/class/Guerrero.png";
            $imgpathClassFuerte = "/imgs/weapons/class/Mago.png";
            $imgpathElementDebil = "/imgs/weapons/element/Agua.png";
            $imgpathElementFuerte = "/imgs/weapons/element/Tierra.png";
            break;
        case ('Incendiario'):
            $imgpath = "/imgs/chars/Mago/Fuego.jpg";
            $imgpathElement = "/imgs/weapons/element/Fuego.png";
            $imgpathClass = "/imgs/weapons/class/Mago.png";
            $imgpathClassDebil = "/imgs/weapons/class/Arquero.png";
            $imgpathClassFuerte = "/imgs/weapons/class/Guerrero.png";
            $imgpathElementDebil = "/imgs/weapons/element/Agua.png";
            $imgpathElementFuerte = "/imgs/weapons/element/Tierra.png";
            break;
        case ('Pirata'):
            $imgpath = "/imgs/chars/Guerrero/Agua.jpg";
            $imgpathElement = "/imgs/weapons/element/Agua.png";
            $imgpathClass = "/imgs/weapons/class/Guerrero.png";
            $imgpathClassDebil = "/imgs/weapons/class/Mago.png";
            $imgpathClassFuerte = "/imgs/weapons/class/Arquero.png";
            $imgpathElementDebil = "/imgs/weapons/element/Viento.png";
            $imgpathElementFuerte = "/imgs/weapons/element/Fuego.png";
            break;
        case ('Avalanchero'):
            $imgpath = "/imgs/chars/Arquero/Agua.jpg";
            $imgpathElement = "/imgs/weapons/element/Agua.png";
            $imgpathClass = "/imgs/weapons/class/Arquero.png";
            $imgpathClassDebil = "/imgs/weapons/class/Guerrero.png";
            $imgpathClassFuerte = "/imgs/weapons/class/Mago.png";
            $imgpathElementDebil = "/imgs/weapons/element/Viento.png";
            $imgpathElementFuerte = "/imgs/weapons/element/Fuego.png";
            break;
        case ('Aguacante'):
            $imgpath = "/imgs/chars/Mago/Agua.jpg";
            $imgpathElement = "/imgs/weapons/element/Agua.png";
            $imgpathClass = "/imgs/weapons/class/Mago.png";
            $imgpathClassDebil = "/imgs/weapons/class/Arquero.png";
            $imgpathClassFuerte = "/imgs/weapons/class/Guerrero.png";
            $imgpathElementDebil = "/imgs/weapons/element/Viento.png";
            $imgpathElementFuerte = "/imgs/weapons/element/Fuego.png";
            break;
        case ('Mercenario'):
            $imgpath = "/imgs/chars/Guerrero/Tierra.png";
            $imgpathElement = "/imgs/weapons/element/Tierra.png";
            $imgpathClass = "/imgs/weapons/class/Guerrero.png";
            $imgpathClassDebil = "/imgs/weapons/class/Mago.png";
            $imgpathClassFuerte = "/imgs/weapons/class/Arquero.png";
            $imgpathElementDebil = "/imgs/weapons/element/Fuego.png";
            $imgpathElementFuerte = "/imgs/weapons/element/Viento.png";
            break;
        case ('Cazador'):
            $imgpath = "/imgs/chars/Arquero/Tierra.png";
            $imgpathElement = "/imgs/weapons/element/Tierra.png";
            $imgpathClass = "/imgs/weapons/class/Arquero.png";
            $imgpathClassDebil = "/imgs/weapons/class/Guerrero.png";
            $imgpathClassFuerte = "/imgs/weapons/class/Mago.png";
            $imgpathElementDebil = "/imgs/weapons/element/Fuego.png";
            $imgpathElementFuerte = "/imgs/weapons/element/Viento.png";
            break;
        case ('Djinn'):
            $imgpath = "/imgs/chars/Mago/Tierra.jpg";
            $imgpathElement = "/imgs/weapons/element/Tierra.png";
            $imgpathClass = "/imgs/weapons/class/Mago.png";
            $imgpathClassDebil = "/imgs/weapons/class/Arquero.png";
            $imgpathClassFuerte = "/imgs/weapons/class/Guerrero.png";
            $imgpathElementDebil = "/imgs/weapons/element/Fuego.png";
            $imgpathElementFuerte = "/imgs/weapons/element/Viento.png";
            break;
        case ('Caballuco'):
            $imgpath = "/imgs/chars/Guerrero/Viento.png";
            $imgpathElement = "/imgs/weapons/element/Viento.png";
            $imgpathClass = "/imgs/weapons/class/Guerrero.png";
            $imgpathClassDebil = "/imgs/weapons/class/Mago.png";
            $imgpathClassFuerte = "/imgs/weapons/class/Arquero.png";
            $imgpathElementDebil = "/imgs/weapons/element/Tierra.png";
            $imgpathElementFuerte = "/imgs/weapons/element/Agua.png";
            break;
        case ('Ronin'):
            $imgpath = "/imgs/chars/Arquero/Viento.png";
            $imgpathElement = "/imgs/weapons/element/Viento.png";
            $imgpathClass = "/imgs/weapons/class/Arquero.png";
            $imgpathClassDebil = "/imgs/weapons/class/Guerrero.png";
            $imgpathClassFuerte = "/imgs/weapons/class/Mago.png";
            $imgpathElementDebil = "/imgs/weapons/element/Tierra.png";
            $imgpathElementFuerte = "/imgs/weapons/element/Agua.png";
            break;
        case ('Tormentador'):
            $imgpath = "/imgs/chars/Mago/Viento.jpg";
            $imgpathElement = "/imgs/weapons/element/Viento.png";
            $imgpathClass = "/imgs/weapons/class/Mago.png";
            $imgpathClassDebil = "/imgs/weapons/class/Arquero.png";
            $imgpathClassFuerte = "/imgs/weapons/class/Guerrero.png";
            $imgpathElementDebil = "/imgs/weapons/element/Tierra.png";
            $imgpathElementFuerte = "/imgs/weapons/element/Agua.png";
            break;       
    }
 
//USANDO LA ZONA DEL MODELO, ELEGIR LAS IMAGENES APROPIADAS   
    switch ($model->nom_zona)
    {
        case ("Desierto"):
            $imgpathZone = "/imgs/zones/wasteland.png";
            $imgpathZoneDebil = "/imgs/zones/cave.jpg";
            break;
        case ("Montaña"):
            $imgpathZone = "/imgs/zones/mountain.jpg";
            $imgpathZoneDebil = "/imgs/zones/wasteland.png";
            break;
        case ("Cueva"):
            $imgpathZone = "/imgs/zones/cave.jpg";
            $imgpathZoneDebil = "/imgs/zones/forest.png";
            break;
        case ("Bosque"):
            $imgpathZone = "/imgs/zones/forest.png";
            $imgpathZoneDebil = "/imgs/zones/mountain.jpg";
            break;
    }
?>

<style>    
.wrapper
{
        outline-style: solid;
        outline-width: 4px;
        outline-color: #0A141B;
        background-color:#0F1F2A;
        width: 900px;
        height: 200px;
        position: relative;
        left: 60px;
}
.leftpane {
    width: 20%;
    height: 100%;
    float: left;
    border-collapse: collapse;
}

.middlepane {
    width: 55%;
    height: 100%;
    border-collapse: collapse;
        outline-style: solid;
        outline-width: 4px;
        outline-color: #0A141B;
}

.rightpane {
  width: 45%;
  height: 100%;
  float: right;
  border-collapse: collapse;

}

.textcolor
{
  color:#fa9632;
}

.textcolor em
{
  color:#fff3b8;
}

.imgleft
{
    border-right: 4px solid;
    border-color:  #0A141B;
}

.imgzone
{
outline-style: solid;
        outline-width: 4px;
        outline-color: #0A141B;
}
</style>

<div class="wrapper">
    <div class="leftpane">
        <img class="imgleft" alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgpath?>" width="200" height="200">
    </div>
        
    <div class="rightpane">
         <h4 class="textcolor"  style="text-align: center;">
             Creado por: <em><?= $model->autor ?></em>
         </h4> 
        <h4 style="border-bottom: 4px solid; border-color: #0A141B;"></h4>
        
        <h4 class="textcolor">
            Fuerte Contra/En:
            <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgpathClassFuerte?>" width="25" height="25"> 
            <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgpathElementFuerte?>" width="20" height="20"> 
            <img class="imgzone" alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgpathZone?>" width="120" height="40"> 
        </h4>
        
        <h4 class="textcolor">
            &nbsp;&nbsp;Débil Contra/En:
            <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgpathClassDebil?>" width="25" height="25"> 
            <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgpathElementDebil?>" width="20" height="20"> 
            <img class="imgzone" alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgpathZoneDebil?>" width="120" height="40"> 
        </h4>
        <?= Html::a('Estadisticas', ['/personajes/estadisticas', 'id' => $model->cod], ['class'=>'btn btn-warning', 'style' => 'width: 150px; height: 45px;']) ?>
    </div>
    
    <div class="middlepane">
        <h3><?= $model->nombre ?></h3>
        <h4 class="textcolor">Nivel: <em><?= $model->nivel ?></em></h4> 
        <h4 class="textcolor">Clase: <em><?= $model->clase ?></em></h4>
        <h4 class="textcolor">
            Tipo:
            <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgpathClass?>" width="25" height="25"> 
            Elemento:
            <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgpathElement?>" width="20" height="20"> 
        </h4>
        <h4 class="textcolor"">Zona Favorita:</h4>
        <img class="imgzone" alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgpathZone?>" width="120" height="40"> 
    </div>

</div>