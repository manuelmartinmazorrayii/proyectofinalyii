<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

//VIEW PARA CREAR EL PERSONAJE USANDO UN ACTIVEFORM

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;


$this->title = 'Crear Personaje';
$url = Yii::getAlias("@web") . '/imgs/others/';
?>

<style>
body {
        background:url(<?= $url ?>bg1.png);
    background-attachment: fixed;
    background-size: cover;
        font-family: 'teen', serif;
}
    
.wrapper
{
        outline-style: solid;
        outline-width: 4px;
        outline-color: #0A141B;
        width: 1000px;
        height: 280px;
        position: relative;
}

</style>

<div class="jumbotron" style="color:#fa9632">
    
    <?php
    if(Yii::$app->session->getFlash('success'))
    {
        echo Yii::$app->session->getFlash('success');
    }
    ?>
    
    <h1>Crear Personaje</h1>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->input("text")->label('Nombre:') ?>

    <?php $listdataclase = ArrayHelper::map($model_clases, 'nombre', 'nombre');?>
    <?= $form->field($model, 'nom_clase')->dropDownList($listdataclase)->label('Elige una Clase:') ?>

    <?php $listdataelemento = ArrayHelper::map($model_elementos, 'nombre', 'nombre');?>
    <?= $form->field($model, 'nom_elemento')->dropDownList($listdataelemento)->label('Elemento:') ?>

    <?php $listdatazonas = ArrayHelper::map($model_zonas, 'nombre', 'nombre');?>
    <?= $form->field($model, 'nom_zona')->dropDownList($listdatazonas)->label('Zona:') ?>

    <?= $form->field($model, 'autor')->input("text")->label('Nombre del Creador:')  ?>

    <div class="form-group">
        <?= Html::submitButton('Crear', ['class' => 'btn btn-warning']) ?>
    </div>
    
</div>

<?php ActiveForm::end(); ?>