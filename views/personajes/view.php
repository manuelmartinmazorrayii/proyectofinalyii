<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\ListView;


$this->title = 'Crear Personaje';
$url = Yii::getAlias("@web") . '/imgs/others/';
?>

<style>
body {
    background:url(<?= $url ?>bg1.png);
    background-attachment: fixed;
    background-size: cover;
}
</style>

<div class="jumbotron" style="color:#fa9632">
    <h1>Personajes</h1>
    
    <div>
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_personajes',
        ]);
        ?>
    </div>
</div>