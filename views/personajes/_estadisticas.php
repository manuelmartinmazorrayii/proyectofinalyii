<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\ListView;


$this->title = 'Crear Personaje';
$url = Yii::getAlias("@web") . '/imgs/others/';

//LAS IMAGENES QUE VAMOS A CARGAR
$imgenemyelement = "";
$imgenemyclass = "";
$imgenemyzone = "";
$resCombate = "";

//ELEGIR DEL ENEMIGO SU ELEMENTO
switch ($model->codjugador2->nom_elemento)
{
    case ('Fuego'):
        $imgenemyelement = "/imgs/weapons/element/Fuego.png";
        break;
    case ('Agua'):
        $imgenemyelement = "/imgs/weapons/element/Agua.png";
        break;
    case ('Viento'):
        $imgenemyelement = "/imgs/weapons/element/Viento.png";
        break;
    case ('Tierra'):
        $imgenemyelement = "/imgs/weapons/element/Tierra.png";
        break;
}

//ELEGIR DEL ENEMIGO SU TIPO
switch ($model->codjugador2->nom_clase)
{
    case ('Guerrero'):
        $imgenemyclass = "/imgs/weapons/class/Guerrero.png";
        break;
    case ('Arquero'):
        $imgenemyclass = "/imgs/weapons/class/Arquero.png";
        break;
    case ('Mago'):
        $imgenemyclass = "/imgs/weapons/class/Mago.png";
        break;
}

//ELEGIR DEL ENEMIGO SU ZONA
switch ($model->codjugador2->nom_zona)
{
    case ('Desierto'):
        $imgenemyzone = "/imgs/zones/wasteland.png";
        break;
    case ('Montaña'):
        $imgenemyzone = "/imgs/zones/mountain.jpg";
        break;
    case ('Cueva'):
        $imgenemyzone = "/imgs/zones/cave.jpg";
        break;
    case ('Bosque'):
        $imgenemyzone = "/imgs/zones/forest.png";
        break;
}

//RECOGER QUIEN ES EL GANADOR
if ($model->ganador == $model->codjugador1->cod)
{
    $resCombate = "WIN";
}
else
{
        $resCombate = "LOSE";
}
?>


<style>    
    
.wrappertexto
{
        border-top: 4px solid;
        border-color:  #0A141B;
        width: 675px;
        height: 30px;
}

.divleft
{
    width: 40%;
    float: left
}
.divleft2
{
    width: 40%;
    float: left
}
.divlefttiny
{
    width: 10%;
    float: left;
}
.divleftmed
{
    width: 10%;
    float: left;
}

.textcolor
{
  color:#fa9632;
}

.textcolor em
{
  color:#fff3b8;
}

.textcolor sep{
    border-right: 4px solid;
}

.textcolor er
{
    color: red;
}
.textcolor gr
{
  color:#fff3b8;
}
</style>
<div class="wrappertexto">
    <div class = "divleft">
        <em><?= $model->codjugador1->nombre?></em> 
        <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgpathClass?>" width="25" height="25"> 
        <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgpathElement?>" width="20" height="20"> 
        <img class="imgzone" alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgpathZona?>" width="60" height="25"> 
    </div>
    <div class="divlefttiny" style="color:#fff3b8">
         &nbsp;VS
    </div>
    <div class="divleft2">
        <?= $model->codjugador2->nombre?>
        <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgenemyclass?>" width="25" height="25"> 
        <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgenemyelement?>" width="20" height="20"> 
        <img class="imgzone" alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgenemyzone?>" width="60" height="25"> 
    </div>
    
    <div class="divleftmed" style="color:#fff3b8">
       &nbsp;&nbsp;<?= $resCombate ?>
    </div>
</div>