<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

//LISTVIEW DEL PERSONAJE QUE EMPEZO LA BATALLA

$this->title = "Personajes";
?>

<style>    
.wrapperluchador
{
        outline-style: solid;
        outline-width: 4px;
        outline-color: #0A141B;
        background-color:#0F1F2A;
        width: 500px;
        height: 220px;
        position: relative;
        left: 300px;
}
.leftpane {
    width: 25%;
    height: 100%;
    float: left;
    border-collapse: collapse;
}

.middlepane {
    width: 60%;
    float: right;
}

.textcolor
{
  color:#fa9632;
}

.textcolor em
{
  color:#fff3b8;
}

.imgleft
{
    border-right: 4px solid;
    border-color:  #0A141B;
}

.imgzone
{
outline-style: solid;
        outline-width: 4px;
        outline-color: #0A141B;
}
</style>

<div class="wrapperluchador">
    
    <div class="leftpane">
        <img class="imgleft" alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgpath?>" width="200" height="220">
    </div>       
    <div class="middlepane">
        <h3><?= $model->nombre ?></h3>
        <h4 class="textcolor">Nivel: <em><?= $model->nivel ?></em></h4> 
        <h4 class="textcolor">Clase: <em><?= $model->clase ?></em></h4>
        <h4 class="textcolor">
            Tipo:
            <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgpathClass?>" width="25" height="25"> 
            Elemento:
            <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgpathElement?>" width="20" height="20"> 
        </h4>
        <h4 class="textcolor"">Zona Favorita:</h4>
        <img class="imgzone" alt="Qries" src="<?php echo Yii::$app->request->baseUrl.$imgpathZone?>" width="120" height="40"> 
    </div>
    <div class="rightpane"></div>

</div>