<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\ListView;

use app\models\Clases;
use app\models\Zonas;
use app\models\Elementos;

use app\controllers\ClasesController;
/* 
 * EN EL ENFRENTAMIENTO VISUALIZAMOS UN PERSONAJE CONTRA OTRO
 */

$url = Yii::getAlias("@web") . '/imgs/zones/';

$zonaRand = mt_rand(1,4);
$imgRand = "";
$zonaNombre = "";

//ELEGIR ALEATORIAMENTE UNA ZONA
switch ($zonaRand)
{
    case (1):
        $imgRand = "wasteland.png";
        $zonaNombre = "Desierto";
        break;
    case (2):
        $imgRand = "mountain.jpg";
        $zonaNombre = "Montaña";
        break;
    case (3):
        $imgRand = "cave.jpg";
        $zonaNombre = "Cueva";
        break;
    case (4):
        $imgRand = "forest.png";
        $zonaNombre = "Bosque";
        break;
}

//CALCULAR FUERZAS
//
$fuerzajugador = 5;
$fuerzaenemigo = 5;
//JUGADOR
if ($model->nivel > $enemymodel->nivel)
{
    $fuerzajugador += 1;
}
if ($clase->nombre == $enemyclase->debilidad)
{
    $fuerzajugador += 1;
    $fuerzaenemigo -= 1;
}
if ($elemento->nombre == $enemyelemento->debilidad)
{
    $fuerzajugador += 2;
    $fuerzaenemigo -= 2;
}
if ($zona->nombre == $zonaNombre)
{
    $fuerzajugador += 2;
}
if ($zona->debilidad == $zonaNombre)
{
    $fuerzajugador -= 1;
}

//ENEMIGO
if ($enemymodel->nivel >$model->nivel)
{
    $fuerzaenemigo += 1;
}
if ($enemyclase->nombre == $clase->debilidad)
{
    $fuerzaenemigo += 1;
    $fuerzajugador -= 1;
}
if ($enemyelemento->nombre == $elemento->debilidad)
{
    $fuerzaenemigo += 2;
    $fuerzajugador -= 2;
}
if ($enemyzona->nombre == $zonaNombre)
{
    $fuerzaenemigo += 2;
}
if ($enemyzona->debilidad == $zonaNombre)
{
    $fuerzaenemigo -= 1;
}
//final de calcular fuerzas

?>

<style>
body {
    background:url(<?= $url ?><?= $imgRand ?>);
    background-attachment: fixed;
    background-size: cover;
        font-family: 'teen', serif;
}

.textcolor
{
  color:#fa9632;
}

.textcolor em
{
  color:#fff3b8;
}

.border
{
    outline-style: solid;
    outline-width: 4px;
    outline-color: #0A141B;
}

.specialborder
{
        outline-style: solid;
        outline-width: 4px;
        outline-color: #0A141B;
        width: 500px;
        position: relative;
        left: 300px;
}
</style>

<div class="jumbotron" style="color:#fa9632">
    
    <div>
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_luchador',
            'viewParams' => ['imgpath' => $imgpath, 
                             'imgpathClass' => $imgpathClass, 
                             'imgpathElement' => $imgpathElement,
                             'imgpathZone' => $imgpathZone],
            'layout' => "{pager}\n{items}",
        ]);
        ?>
    </div>
        <h5>&nbsp;</h5>
    <div class="specialborder" style="background-color:#0F1F2A;">
    <h1>VS.</h1>
    </div>
    <h5>&nbsp;</h5>
    
    <div>
        <?= ListView::widget([
            'dataProvider' => $enemydataProvider,
            'itemView' => '_contrincante',
            'viewParams' => ['enemyimgpath' => $enemyimgpath, 
                             'enemyimgpathClass' => $enemyimgpathClass, 
                             'enemyimgpathElement' => $enemyimgpathElement],
            'layout' => "{pager}\n{items}",
        ]);
        ?>
    </div>
    
    <div class="specialborder" style="background-color:#0F1F2A;">
    <h3 class="textcolor">Zona de batalla: <em><?php echo $zonaNombre; ?></em></h3>
    
    <h3 class="textcolor">Tu fuerza: <em><?php echo $fuerzajugador; ?></em></h3>
    <h3 class="textcolor">
        Fuerza enemiga:
        <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.'/imgs/others/unknown.png'?>" width="25" height="25"> 
    </h3>
    
    <h3 class="textcolor">Elige una opción:</h3>
    <?= Html::a('Luchar!', ['/personajes/resultadocombate',
                            'id1' => $model->cod,
                            'id2' => $enemymodel->cod,
                            'fuerza1' => $fuerzajugador,
                            'fuerza2' => $fuerzaenemigo,
                            'rendido' => false], 
    ['class'=>'btn btn-success', 'style' => 'width: 150px; height: 50px;' ]) ?>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <?= Html::a('Huir!', ['/personajes/resultadocombate',
                            'id1' => $model->cod,
                            'id2' => $enemymodel->cod,
                            'fuerza1' => $fuerzajugador,
                            'fuerza2' => $fuerzaenemigo,
                            'rendido' => true], 
    ['class'=>'btn btn-danger', 'style' => 'width: 150px; height: 50px;' ]) ?>
    
    
    </div>
</div>