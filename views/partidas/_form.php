<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Partidas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="partidas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ganador')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'perdedor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cod_jugador1')->textInput() ?>

    <?= $form->field($model, 'cod_jugador2')->textInput() ?>

    <?= $form->field($model, 'matanza')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
