<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Characters';
?>

<style>
    body {
    background-color: #20475D;
}
</style>

<div class="jumbotron">
    <h1 style="color:#fa9632"><?= Html::encode($this->title) ?></h1>
       <p>    <?= Html::a('Crear Personaje', ['/personajes/crearpersonaje'], ['class'=>'btn btn-success']) ?> </p>
    
    <h3 style="color:#fa9632">Search</h3>
    <p>
        <a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Search users</a>
        <a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Search Characters</a>
    </p>
</div>