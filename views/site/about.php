<?php

/* @var $this yii\web\View */
//LA INFORMACION RELEVANTE PARA APRENDER A JUGAR AL JUEGO

use yii\helpers\Html;

$this->title = 'Info';
$url = Yii::getAlias("@web") . '/imgs/others/';
?>
<style>
    body 
    {
    background:url(<?= $url ?>bg1.png);
    background-attachment: fixed;
    background-size: cover;
        font-family: 'teen', serif;
    }
.wrapper
{
    outline-style: solid;
    outline-width: 4px;
    outline-color: #0A141B;
    width: 1140px;
    height: 150px;
}

.wrappertall
{
    outline-style: solid;
    outline-width: 4px;
    outline-color: #0A141B;
    width: 1140px;
    height: 525px;
}

.wrappertallextra
{
    outline-style: solid;
    outline-width: 4px;
    outline-color: #0A141B;
    width: 1140px;
    height: 625px;
}
.leftpane {
    width: 30%;
    height: 40%;
    float: left;
    border-collapse: collapse;
}

.rightpane{
  width: 30%;
  height: 40%;
  float: right;
  border-collapse: collapse;
}

.middlepane{
  width: 25%;
  height: 40%;
  border-collapse: collapse;
}
    
.title {
  color:#fa9632;
  text-align: center;
}

.text {
  color:#fff3b8;
  text-align: center;
}

.imageborder
{
    border: 4px solid;
    border-color:  #0A141B;
}

.imageborderL
{
    border-left: 4px solid;
    border-color:  #0A141B;
}

.imageborderR
{
    border-right: 4px solid;
    border-color:  #0A141B;
}

.imgzone
{
outline-style: solid;
        outline-width: 4px;
        outline-color: #0A141B;
}
</style>

<div class="site-about" >
    
    <div class="wrapper" style="background-color:#0F1F2A;">
            <h1 class="title">Que es Carácter Creator Online?</h1>
            <p class="text">En Carácter creator online crearas tus propios personajes y los enfrentarar contra otros personajes en una batalla de suerte!</p>
            <p class="text">Tus elecciones determinaran la clase de tu personaje y las ventajas/desventajas que tendras sobre tus contrincantes!</p>
            <p class="text">Como de lejos llegara tu personaje?</p>
    </div>
    
    <div class ="wrappertall" style="background-color:#0F1F2A;">

            <h1 class="title">Crear tu personaje</h1>
            <p class="text">Para empezar, tendras que crear un personaje! Para crear un personaje, tendras que elegir que tipo de personaje sera</p>
            <p class="text">Podras elegir entre los siguientes tipos:</p>
            <div style="display: flex; align-items: center; justify-content: center;">
            <p class="text">
                Arquero: <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.'/imgs/weapons/class/Arquero.png'?>" width="40" height="40"> 
                Guerrero: <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.'/imgs/weapons/class/Guerrero.png'?>" width="40" height="40"> 
                Mago: <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.'/imgs/weapons/class/Mago.png'?>" width="40" height="40"> 
            </p>
            </div>
            
            <h1></h1>
            <p class="text">Despues debes elegir uno de los 4 elementos:</p>
            <div style="display: flex; align-items: center; justify-content: center;">
            <p class="text">
                Agua: <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.'/imgs/weapons/element/Agua.png'?>" width="30" height="30"> 
                Fuego: <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.'/imgs/weapons/element/Fuego.png'?>" width="30" height="30"> 
                Tierra: <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.'/imgs/weapons/element/Tierra.png'?>" width="30" height="30"> 
                Viento: <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.'/imgs/weapons/element/Viento.png'?>" width="30" height="30"> 
            </p>
            </div>
            
            <h1></h1>
            <p class="text">Finalmente eligiras una zona en la que tu personaje sera eficaz:</p>
            <div style="display: flex; align-items: center; justify-content: center;">
            <p class="text">
                Bosque:&nbsp;  <img class="imgzone" alt="Qries" src="<?php echo Yii::$app->request->baseUrl.'/imgs/zones/forest.png'?>" width="120" height="40"> 
                &nbsp;&nbsp;Montaña:&nbsp; <img class="imgzone" alt="Qries" src="<?php echo Yii::$app->request->baseUrl.'/imgs/zones/mountain.jpg'?>" width="120" height="40"> 
                &nbsp;&nbsp;Desierto:&nbsp; <img class="imgzone" alt="Qries" src="<?php echo Yii::$app->request->baseUrl.'/imgs/zones/wasteland.png'?>" width="120" height="40"> 
                &nbsp;&nbsp;Cueva:&nbsp; <img class="imgzone" alt="Qries" src="<?php echo Yii::$app->request->baseUrl.'/imgs/zones/cave.jpg'?>" width="120" height="40"> 
            </p>
            </div>
            
            <h1></h1>
            <p class="text">Una vez que hayas terminado tu personaje, obtendras una clase dependiendo del tipo y del elemento que elegiste!</p>
            <div style="display: flex; align-items: center; justify-content: center;">
            <p class="text">
                <img class="imgzone" alt="Qries" src="<?php echo Yii::$app->request->baseUrl.'/imgs/chars/Arquero/Agua.jpg'?>" width="100" height="100"> 
                <img class="imgzone" alt="Qries" src="<?php echo Yii::$app->request->baseUrl.'/imgs/chars/Guerrero/Fuego.png'?>" width="100" height="100"> 
                <img class="imgzone" alt="Qries" src="<?php echo Yii::$app->request->baseUrl.'/imgs/chars/Mago/Tierra.jpg'?>" width="100" height="100"> 
                <img class="imgzone" alt="Qries" src="<?php echo Yii::$app->request->baseUrl.'/imgs/chars/Guerrero/Viento.png'?>" width="100" height="100"> 
            </p>
            </div>
    </div>
    
    <h1></h1>
    
    <div class="wrappertallextra" style="background-color:#0F1F2A;">
        
        <h1 class="title">Luchar contra otros jugadores</h1>
        <p class="text">
        Cuando creas tu personaje, se añade a la base de datos de personajes. Podras darle al boton de "Luchar!" para pelear contra otros personajes!
        Pelearas contra un personaje aleatorio, en una de las 4 zonas aleatoriamente!
        </p>
        
        <h1 class="title">Fortalezas & Debilidades</h1>
        <p class="text">
        Para mejorar tus oportunidades de ganar, tu tipo y tu elemento elegido te daran ventajas dependiendo 
        de el tipo y el elemento de tu enemigo! Cada tipo es fuerte contra un tipo, y debil contra el otro. 
        Cada elemento es fuerte contra un elemento, y debil contra otro. Ademas, seras mas fuerte en la 
        zona que elegiste al crear tu personaje, pero la zona que eliges causara que seas debil en otra zona.
        </p>

        <div class="leftpane">
        <h3 class="title">Tipos</h3>
        <div style="display: flex; align-items: center; justify-content: center;">
        <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.'/imgs/others/ClasesWeak.png'?>" width="175" height="175"> 
        </div>
        <h1 class="title"> </h1>
        <p class="text">
        Los Guerreros son fuertes contra los Arqueros
        </p>
        <p class="text">
        Los Arqueros son fuertes contra los Magos
        </p>
        <p class="text">
        Los Magos son fuertes contra los Guerreros
        </p>
        </div>
        
        <div class="rightpane">  
        <h3 class="title">Zonas</h3>
        <div style="display: flex; align-items: center; justify-content: center;">
        <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.'/imgs/others/ZonasWeak.png'?>" width="275" height="175"> 
        </div>
        <h1 class="title"> </h1>
        <p class="text">
        Personajes de Bosques son debiles en Montañas
        </p>
        <p class="text">
        Personajes de Montañas son debiles en Desiertos
        </p>
        <p class="text">
        Personajes de Desiertos son debiles en Cuevas
        </p>
        <p class="text">
        Personajes de Cuevas son debiles en Bosques
        </p> 
        </div>
        
        <div> 
        <h1></h1>
        <h3 class="title">Elementos</h3>
        <div style="display: flex; align-items: center; justify-content: center;">
        <img alt="Qries" src="<?php echo Yii::$app->request->baseUrl.'/imgs/others/ElementosWeak.png'?>" width="175" height="175"> 
        </div>
        <h1 class="title"> </h1>
        <p class="text">
        El agua es fuerte contra el fuego
        </p>
        <p class="text">
        El fuego es fuerte contra la tierra
        </p>
        <p class="text">
        La tierra es fuerte contra el viento
        </p>
        <p class="text">
        El viento es fuerte contra el agua
        </p>          
        </div>
    </div>
        
    <div class ="wrappertallextra" style="background-color:#0F1F2A;">  
        <div>
        <h1 class="title">Puntuacion</h1>          
        <p class="text">
        Los combates se deciden por puntos de fuerza, cuanta mas fuerza tengas, mas alta sera tu posibilidad de ganar la lucha!
        Ambos jugadores empiezan con 5 de fuerza, pero su tipo, elemento y zona preferida mejoran sus puntos de fuerza!
        </p>
        <h1></h1>
            <p class="text">Si un jugador tiene un tipo que venze al del otro jugador, gana 1 punto y el otro pierdo 1 punto.</p>     
            <p class="text">Si un jugador tiene un elemento que venze al del otro jugador, gana 2 punto y el otro pierde 2 puntos.</p>      
            <p class="text">Si un jugador esta en la zona en la que es eficaz, gana 2 puntos, si es la zona en la que es debil, pierde 1 punto</p>                    
            <p class="text">finalmente, si tu nivel es mas alto que el de el enemigo, ganas 1 punto de fuerza.</p>
          <h1></h1>  
            <p class="text">
                todas estas reglas cuentan para ti y para tu contrincante
            </p>
        </div>
        <div>
        <h1 class="title">El Combate</h1>
        <p class="text">Cuando empiezas un combate, te enfrentaras contra un enemigo aleatorio, en una de las 4 zonas aleatoriamente.</p>
        <p class="text">Antes de luchar, podras ver la informacion de tu personaje, y informacion limitada de tu enemigo. 
           Tambien podras ver tus puntos de fuerza</p>
        <p class="text">con la informacion que tienes, tendras que decidir si quieres luchar o huir!</p>
        <p class="text">Si luchas, se calcularan los puntos de fuerza de ambos jugadores! y ganaras o perderas</p>
        <p class="text">Si huyes, podras escapar con la oportunidad de no perder niveles!</p>
        
        <h1 class="title">Victoria o Derrota?</h1>
        <p class="text">El ganador ganara niveles dependiendo de cuanta oportunidad tenia de ganar</p>
        <p class="text">El perdedor perdera niveles dependiendo de cuanta oportunidad tenia de ganar</p>
        <p class="text">Ademas, si el perdedor llega a nivel 0, se eliminara del juego!</p>
        </div>
    </div>
    
    <h1></h1>
    <div class ="wrapper" style="background-color:#0F1F2A;">
        <h1 class="title">Preparate para la aventura!</h1>
        <p class="text">
            No estas obligado a usar un solo personaje, puedes crear cuantos quieras y pelear cuantas veces quieras! Los podras visitar en la lista de personajes! Preparate para la lucha!
        </p>
        <div style="display: flex; align-items: center; justify-content: center;">
         
                <?= Html::a('Crear Personaje!', ['/personajes/crearpersonaje'], ['class'=>'btn btn-warning', 'style' => 'width: 150px; height: 35px;' ]) ?>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <?= Html::a('Ver Personajes', ['/personajes/personajes'], ['class'=>'btn btn-warning', 'style' => 'width: 150px; height: 35px;' ]) ?>
        </div>
    </div>
</div>