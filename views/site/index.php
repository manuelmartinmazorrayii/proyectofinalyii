<?php
//PAGINA PRINCIPAL
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use app\models\Personajes;

$this->title = 'My Yii Application';
$url = Yii::getAlias("@web") . '/imgs/others/';

?>

<style>
body {
    background:url(<?= $url ?>bg1.png);
    background-attachment: fixed;
    background-size: cover;
    font-family: 'teen', serif;
}


.middlepane
{
        width: 500px;
        height: 100%;
        border-collapse: collapse;
}

.divider
{
    float: left;
    width: 50%;
    height: 50%;
}

.wrapper
{
    outline-style: solid;
    outline-width: 4px;
    outline-color: #0A141B;
    width: 600px;
    height: 325px;
    background-color:#0F1F2A;
}

.imageborder
{
    border: 4px solid;
    border-color:  #0A141B;
    width: 200px;
    height: 200px;
}
.imageborderzone
{
    width: 200px;
    height: 100px;
}
</style>

<div class="site-index">

    <div class="jumbotron">
        <h1 style="color:#fa9632">Carácter Creator Online</h1>

        <p class="lead" style="color:#fff3b8">el creador de personajes de aventura</p>
         <div style="display: flex; align-items: center; justify-content: center;">    
        <div class="middlepane">
                <img class="imageborder" src="<?php echo Yii::$app->request->baseUrl.'/imgs/chars/Guerrero/Viento.png'?>" width="200" height="200">
                <img class="imageborder" src="<?php echo Yii::$app->request->baseUrl.'/imgs/chars/Guerrero/Fuego.png'?>" width="200" height="200">
                
                <img class="imageborder" src="<?php echo Yii::$app->request->baseUrl.'/imgs/chars/Mago/Tierra.jpg'?>" width="200" height="200">
                <img class="imageborder" src="<?php echo Yii::$app->request->baseUrl.'/imgs/chars/Guerrero/Agua.jpg'?>" width="200" height="200">
            </div>
         </div>      
        <h1></h1>
        
        <div style="display: flex; align-items: center; justify-content: center;">  
            <div class='wrapper'>
                <h1></h1>
                <div class="divider"> 

                        
                <?= Html::a(
                    Html::img('@web/imgs/others/btnCreate.png', ['class' => 'imageborderzone']),
                    ['/personajes/crearpersonaje']
                )?>
                    <p style="color:#fff3b8">       
                        Crear Personaje
                    </p>
                </div>
                
                                
                <div class="divider"> 
                     
                 &nbsp;     
                <?= Html::a(
                    Html::img('@web/imgs/others/btnLearn.png', ['class' => 'imageborderzone']),
                    ['/site/about']
                )?>
                    <p style="color:#fff3b8">   
                        Como Jugar
                    </p>
                </div>
                
                <div class="divider"> 
                
                   
                <?= Html::a(
                    Html::img('@web/imgs/others/btnChars.png', ['class' => 'imageborderzone']),
                    ['/personajes/personajes']
                )?>
                   <p style="color:#fff3b8"> 
                    Personajes
                    </p>
                </div>
                
                <div class="divider"> 
                    <p style="color:#fff3b8"> 
                 &nbsp;       
                <?= Html::a(
                    Html::img('@web/imgs/others/btnGrave.png', ['class' => 'imageborderzone']),
                    ['/personajes/cementerio']
                )?>
                        Cementerio
                    </p>
                </div>
            </div>
            
        </div>
    </div>
    
</div>