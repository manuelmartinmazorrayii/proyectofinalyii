<?php
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use app\models\Personajes;

$dataProvider = new ActiveDataProvider([
    
   'query' => Personajes::find(),
   'pagination' => false,
   'sort' =>
    [
      //  'attributes' => ['puntuacion'],
        'defaultOrder' => ['puntuacion' => SORT_DESC],
    ]
    
]);
?>

<h2>Mejores Jugadores</h2>
<?= GridView::widget([
    
    'dataProvider' => $dataProvider,
    //'showFooter'=>false,
    //'showHeader'=>true,
    'columns' => [
        'nom_personaje',
        'puntuacion',
        'nom_arma',
        'nom_armadura',
        'nom_usuario'
    ],
    
]); ?>