<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partidas".
 *
 * @property int $cod
 * @property int $ganador
 * @property int $perdedor
 * @property int $cod_jugador1
 * @property int $cod_jugador2
 * @property int|null $matanza
 *
 * @property Personajes $codJugador1
 * @property Personajes $codJugador2
 */
class Partidas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partidas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ganador', 'perdedor', 'cod_jugador1', 'cod_jugador2'], 'required'],
            [['ganador', 'perdedor', 'cod_jugador1', 'cod_jugador2', 'matanza'], 'integer'],
            [['cod_jugador1'], 'exist', 'skipOnError' => true, 'targetClass' => Personajes::className(), 'targetAttribute' => ['cod_jugador1' => 'cod']],
            [['cod_jugador2'], 'exist', 'skipOnError' => true, 'targetClass' => Personajes::className(), 'targetAttribute' => ['cod_jugador2' => 'cod']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'ganador' => 'Ganador',
            'perdedor' => 'Perdedor',
            'cod_jugador1' => 'Cod Jugador1',
            'cod_jugador2' => 'Cod Jugador2',
            'matanza' => 'Matanza',
        ];
    }

    /**
     * Gets query for [[CodJugador1]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodjugador1()
    {
        return $this->hasOne(Personajes::className(), ['cod' => 'cod_jugador1']);
    }

    /**
     * Gets query for [[CodJugador2]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodjugador2()
    {
        return $this->hasOne(Personajes::className(), ['cod' => 'cod_jugador2']);
    }
}
