<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "personajes".
 *
 * @property int $cod
 * @property string|null $nombre
 * @property string|null $autor
 * @property int|null $nivel
 * @property string|null $clase
 * @property string|null $cod_partidas
 * @property string|null $nom_clase
 * @property string|null $nom_elemento
 * @property string|null $nom_zona
 * @property int|null $muerto
 *
 * @property Partidas[] $partidas
 * @property Partidas[] $partidas0
 * @property Clases $nomClase
 * @property Elementos $nomElemento
 * @property Zonas $nomZona
 */
class Personajes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'personajes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nivel', 'muerto'], 'integer'],
            [['nivel'],'default','value'=>'5'],
            [['muerto'],'default','value'=>'0'],
            [['nombre', 'autor', 'nom_clase', 'nom_elemento', 'nom_zona'], 'required'],
            [['nombre'], 'string', 'max' => 10],
            [['autor',  'clase'], 'string', 'max' => 15],
            [['cod_partidas'], 'string', 'max' => 9],
            [['nom_clase', 'nom_elemento', 'nom_zona'], 'string', 'max' => 20],
            [['nom_clase'], 'exist', 'skipOnError' => true, 'targetClass' => Clases::className(), 'targetAttribute' => ['nom_clase' => 'nombre']],
            [['nom_elemento'], 'exist', 'skipOnError' => true, 'targetClass' => Elementos::className(), 'targetAttribute' => ['nom_elemento' => 'nombre']],
            [['nom_zona'], 'exist', 'skipOnError' => true, 'targetClass' => Zonas::className(), 'targetAttribute' => ['nom_zona' => 'nombre']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'nombre' => 'Nombre',
            'autor' => 'Autor',
            'nivel' => 'Nivel',
            'clase' => 'Clase',
            'cod_partidas' => 'Cod Partidas',
            'nom_clase' => 'Nom Clase',
            'nom_elemento' => 'Nom Elemento',
            'nom_zona' => 'Nom Zona',
            'muerto' => 'Muerto',
        ];
    }

    /**
     * Gets query for [[Partidas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPartidas()
    {
        return $this->hasMany(Partidas::className(), ['cod_jugador1' => 'cod']);
    }

    /**
     * Gets query for [[Partidas0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPartidas0()
    {
        return $this->hasMany(Partidas::className(), ['cod_jugador2' => 'cod']);
    }

    /**
     * Gets query for [[NomClase]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNomClase()
    {
        return $this->hasOne(Clases::className(), ['nombre' => 'nom_clase']);
    }

    /**
     * Gets query for [[NomElemento]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNomElemento()
    {
        return $this->hasOne(Elementos::className(), ['nombre' => 'nom_elemento']);
    }

    /**
     * Gets query for [[NomZona]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNomZona()
    {
        return $this->hasOne(Zonas::className(), ['nombre' => 'nom_zona']);
    }
    
    public function actionSelecttipo($id)
    {
        if ($id == 'Arquero')
        {
            return "/imgs/weapons/class/Arquero.png";
        }
        if ($id == 'Guerrero')
        {
            return "/imgs/weapons/class/Guerrero.png";
        }
        if ($id == 'Mago')
        {
            return "/imgs/weapons/class/Mago.png";
        }
    }
    public function actionSelecttipofuerte($id)
    {
        if ($id == 'Arquero')
        {
            return "/imgs/weapons/class/Mago.png";
        }
        if ($id == 'Guerrero')
        {
            return "/imgs/weapons/class/Arquero.png";
        }
        if ($id == 'Mago')
        {
            return "/imgs/weapons/class/Guerrero.png";
        }
    }
    public function actionSelecttipodebil($id)
    {
        if ($id == 'Arquero')
        {
            return "/imgs/weapons/class/Guerrero.png";
        }
        if ($id == 'Guerrero')
        {
            return "/imgs/weapons/class/Mago.png";
        }
        if ($id == 'Mago')
        {
            return "/imgs/weapons/class/Arquero.png";
        }
    }
    
    public function actionSelectelement($id)
    {
        if ($id == 'Fuego')
        {
            return "/imgs/weapons/element/Fuego.png";
        }
        if ($id == 'Agua')
        {
            return "/imgs/weapons/element/Agua.png";
        }
        if ($id == 'Tierra')
        {
            return "/imgs/weapons/element/Tierra.png";
        }
         if ($id == 'Viento')
        {
            return "/imgs/weapons/element/Viento.png";
        }
    }
    public function actionSelectelementfuerte($id)
    {
        if ($id == 'Fuego')
        {
            return "/imgs/weapons/element/Tierra.png";
        }
        if ($id == 'Agua')
        {
            return "/imgs/weapons/element/Fuego.png";
        }
        if ($id == 'Tierra')
        {
            return "/imgs/weapons/element/Viento.png";
        }
         if ($id == 'Viento')
        {
            return "/imgs/weapons/element/Agua.png";
        }
    }
    public function actionSelectelementdebil($id)
    {
        if ($id == 'Fuego')
        {
            return "/imgs/weapons/element/Agua.png";
        }
        if ($id == 'Agua')
        {
            return "/imgs/weapons/element/Viento.png";
        }
        if ($id == 'Tierra')
        {
            return "/imgs/weapons/element/Fuego.png";
        }
         if ($id == 'Viento')
        {
            return "/imgs/weapons/element/Tierra.png";
        }
    }
    
    public function actionSelectclase($id)
    {
        switch ($id)
        {
        case ('Herrero'):
            $imgpath = "/imgs/chars/Guerrero/Fuego.png";
            break;
        case ('Caza-Dragones'):
            $imgpath = "/imgs/chars/Arquero/Fuego.jpg";
            break;
        case ('Incendiario'):
            $imgpath = "/imgs/chars/Mago/Fuego.jpg";
            break;
        case ('Pirata'):
            $imgpath = "/imgs/chars/Guerrero/Agua.jpg";
            break;
        case ('Avalanchero'):
            $imgpath = "/imgs/chars/Arquero/Agua.jpg";
            break;
        case ('Aguacante'):
            $imgpath = "/imgs/chars/Mago/Agua.jpg";
            break;
        case ('Mercenario'):
            $imgpath = "/imgs/chars/Guerrero/Tierra.png";
            break;
        case ('Cazador'):
            $imgpath = "/imgs/chars/Arquero/Tierra.png";
            break;
        case ('Djinn'):
            $imgpath = "/imgs/chars/Mago/Tierra.jpg";
            break;
        case ('Caballuco'):
            $imgpath = "/imgs/chars/Guerrero/Viento.png";
            break;
        case ('Ronin'):
            $imgpath = "/imgs/chars/Arquero/Viento.png";
            break;
        case ('Tormentador'):
            $imgpath = "/imgs/chars/Mago/Viento.jpg";
            break;
        }
        return $imgpath;
    }
    
    public function actionSelectzona($id)
    {    
        switch ($id)
        {
            case ("Desierto"):
                $imgpath = "/imgs/zones/wasteland.png";
                break;
            case ("Montaña"):
                $imgpath = "/imgs/zones/mountain.jpg";
                break;
            case ("Cueva"):
                $imgpath = "/imgs/zones/cave.jpg";
                break;
            case ("Bosque"):
                $imgpath = "/imgs/zones/forest.png";
                break;
        }
        return $imgpath;
    }
}
