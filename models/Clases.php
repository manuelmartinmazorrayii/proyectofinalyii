<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clases".
 *
 * @property string $nombre
 * @property string|null $fuerza
 * @property string|null $debilidad
 *
 * @property Personajes[] $personajes
 */
class Clases extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clases';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre', 'fuerza', 'debilidad'], 'string', 'max' => 20],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre',
            'fuerza' => 'Fuerza',
            'debilidad' => 'Debilidad',
        ];
    }

    /**
     * Gets query for [[Personajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonajes()
    {
        return $this->hasMany(Personajes::className(), ['nom_clase' => 'nombre']);
    }
}
