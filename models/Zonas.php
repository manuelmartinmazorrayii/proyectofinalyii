<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "zonas".
 *
 * @property string $nombre
 * @property string|null $debilidad
 *
 * @property Personajes[] $personajes
 */
class Zonas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'zonas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre', 'debilidad'], 'string', 'max' => 20],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre',
            'debilidad' => 'Debilidad',
        ];
    }

    /**
     * Gets query for [[Personajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonajes()
    {
        return $this->hasMany(Personajes::className(), ['nom_zona' => 'nombre']);
    }
}
