<?php

//EN PERSONAJESCONTROLLER ESTA VIRTUALMENTE TODA LA FUNCIONALIDAD QUE HE CREADO, AQUI PODRAS VER TODAS LAS FUNCIONES CREADAS

namespace app\controllers;

use Yii;
use app\models\Personajes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Expression;

use app\models\Clases;
use app\models\Zonas;
use app\models\Elementos;
use \app\models\Partidas;
use yii\db\Query;

/**
 * PersonajesController implements the CRUD actions for Personajes model.
 */
class PersonajesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Personajes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Personajes::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Personajes model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
       /*
    /**
     * Creates a new Personajes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Personajes();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->cod]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Personajes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->cod]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Personajes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Personajes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Personajes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Personajes::findOne($id)) !== null) {
            return $model;
        }

        return null;
    }
    
    //MOSTRAR TODOS LOS PERSONAJES
        public function actionPersonajes()
    {     
        //buscar todos los personajes que esten vivos
        $dataProvider = new ActiveDataProvider([
            'query' => Personajes::find()->where(['muerto' => 0]),
        ]);
        
        return $this->render('personajes', [
            'dataProvider' => $dataProvider,
        ]);
    }
    //MOSTRAR TODOS LOS PERSONAJES
        public function actionEstadisticas($id)
    {
        $mymodel = $this->findModel($id);
        $imgpath = $this->actionSelectclase($mymodel->clase);
        
        //IMAGENES PARA LOS ELEMENTOS
        $imgpathElement = $this->actionSelectelement($mymodel->nom_elemento);
        $imgpathElementdebil = $this->actionSelectelementdebil($mymodel->nom_elemento);
        $imgpathElementfuerte = $this->actionSelectelementfuerte($mymodel->nom_elemento);
        
        //IMAGENES PARA LOS TIPOS
        $imgpathClass = $this->actionSelecttipo($mymodel->nom_clase);
        $imgpathClassdebil = $this->actionSelecttipodebil($mymodel->nom_clase);
        $imgpathClassfuerte = $this->actionSelecttipofuerte($mymodel->nom_clase);
        
        //IMAGENES PARA LAS ZONAS
        $imgpathZona = $this->actionSelectzona($mymodel->nom_zona);
        $imgpathZonadebil = $this->actionSelectzonadebil($mymodel->nom_zona);
        
        
        //DATA PROVIDER DE HISTORIAL DE COMBATES
        $dataProvider = new ActiveDataProvider([
            'query' => Partidas::find()
                    ->alias('p')
                    ->innerJoinWith('codjugador1','(personajes.cod = p.cod_jugador1 OR personajes.cod = p.cod_jugador2)')->alias('p1')
                    ->innerJoinWith('codjugador2', '(p1.cod = p.cod_jugador1 AND personajes.cod = p.cod_jugador2 OR p1.cod = p.cod_jugador2 AND personajes.cod = p.cod_jugador1)')
                    ->where(['cod_jugador1' => $mymodel->cod])->orderBy(['p1.cod' => SORT_DESC])->limit(5),
             'pagination' => false,
        ]);
        
        //LA CANTIDAD DE VICTORIAS OBTENIDAS POR ESTE PERSONAJE
        $victorias = new ActiveDataProvider([
            'query' => Partidas::find()->where(['ganador' => $mymodel->cod])
                ]);
        $victories = (int) $victorias->count;
        
        //LA CANTIDAD DE ENEMIGOS MATADOS POR ESTE PERSONAJE
        $matanzas = new ActiveDataProvider([
            'query' => Partidas::find()->where(['ganador' => $mymodel->cod])
                       ->andWhere(['matanza' => 1])
                ]);
        $kills = (int) $matanzas->count;
        
        //LA CANTIDAD DE PARTIDAS PERDIDAS POR ESTE PERSONAJE
        $perdidas = new ActiveDataProvider([
            'query' => Partidas::find()
                ->where("ganador != $mymodel->cod")->andWhere(['cod_jugador1' => $mymodel->cod])
                ->orWhere(['cod_jugador2' => $mymodel->cod])->andWhere("ganador != $mymodel->cod")
                ]);
        $defeats =  (int) $perdidas->count;
        
        //EL WINRATE (PORCENTAJE DE VICTORIAS)
        $countgames = $defeats + $victories;
        if ($countgames != 0)
        {
        $calcWr = $victories / ($countgames) * 100;
        $winrate = number_format($calcWr, 2);
        }
        else
        {
            $winrate = 0;
        }     
        
        //EL PERSONAJE QUE MAS TE HA VENCIDO EN COMBATES
        $nemesis = "";
        $findnemesis = new ActiveDataProvider([
           'query' => Partidas::find()->select('ganador')
                ->where("ganador != $mymodel->cod")->andWhere(['cod_jugador1' => $mymodel->cod])
                ->orWhere(['cod_jugador2' => $mymodel->cod])->andWhere("ganador != $mymodel->cod")
                ->groupBy('ganador')->orderBy('COUNT(ganador)')->limit(1)
        ]);
        $newQuery = clone $findnemesis->query;
        $cloneres = $newQuery->limit(1)->one();
        if ($cloneres)
        $nemesis = $this->findModel($cloneres->ganador)->nombre;
        
        //EL PERSONAJE QUE MAS HAS VENCIDO TU EN COMBATE
        $victima = "";
        $findvictima = new ActiveDataProvider([
           'query' => Partidas::find()->select('perdedor')
                ->where("perdedor != $mymodel->cod")->andWhere(['cod_jugador1' => $mymodel->cod])
                ->orWhere(['cod_jugador2' => $mymodel->cod])->andWhere("perdedor != $mymodel->cod")
                ->groupBy('perdedor')->orderBy('COUNT(perdedor)')->limit(1)
        ]);
        $newQuery2 = clone $findvictima->query;
        $cloneres2 = $newQuery2->limit(1)->one();
        if ($cloneres2)
        $victima = $this->findModel($cloneres2->perdedor)->nombre;
        
        //RENDERIZAR Y PASAR TODOS LOS VARIABLES RELEVANTES
        return $this->render('estadisticas', [
            'mymodel' =>$mymodel,
            'victories' =>$victories,
            'kills' =>$kills,
            'defeats' =>$defeats,
            'winrate' =>$winrate,
            'nemesis' =>$nemesis,
            'victima' =>$victima,
            'dataProvider' => $dataProvider,
            'imgpath' => $imgpath,
            'imgpathElement' => $imgpathElement,
            'imgpathElementdebil' => $imgpathElementdebil,
            'imgpathElementfuerte' => $imgpathElementfuerte,
            'imgpathClass' => $imgpathClass,
            'imgpathClassdebil' => $imgpathClassdebil,
            'imgpathClassfuerte' => $imgpathClassfuerte,
            'imgpathZona' => $imgpathZona,
            'imgpathZonadebil' => $imgpathZonadebil,
        ]);
    }
    
    
        //MOSTRAR TODOS LOS MUERTOS
        public function actionCementerio()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Personajes::find()->where(['muerto' => 1]),
        ]);
        
        return $this->render('cementerio', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    //MOSTRAR PERSONAJE CREADO
        public function actionPersonajesingular($id)
    {
        $model = $this->findModel($id);
        $imgpath = $this->actionSelectclase($model->clase);
        
        $imgpathElement = $this->actionSelectelement($model->nom_elemento);
        $imgpathElementdebil = $this->actionSelectelementdebil($model->nom_elemento);
        $imgpathElementfuerte = $this->actionSelectelementfuerte($model->nom_elemento);
        
        $imgpathClass = $this->actionSelecttipo($model->nom_clase);
        $imgpathClassdebil = $this->actionSelecttipodebil($model->nom_clase);
        $imgpathClassfuerte = $this->actionSelecttipofuerte($model->nom_clase);
        
        $imgpathZona = $this->actionSelectzona($model->nom_zona);
        $imgpathZonadebil = $this->actionSelectzonadebil($model->nom_zona);
        
        $dataProvider = new ActiveDataProvider([
            'query' => Personajes::find()->where(['cod' => $model->cod])
        ]);
        
        return $this->render('personajesingular', [
            'dataProvider' => $dataProvider,
            'imgpath' => $imgpath,
            'imgpathElement' => $imgpathElement,
            'imgpathElementdebil' => $imgpathElementdebil,
            'imgpathElementfuerte' => $imgpathElementfuerte,
            'imgpathClass' => $imgpathClass,
            'imgpathClassdebil' => $imgpathClassdebil,
            'imgpathClassfuerte' => $imgpathClassfuerte,
            'imgpathZona' => $imgpathZona,
            'imgpathZonadebil' => $imgpathZonadebil,
        ]);
    }
    
    //MOSTRAR UN ENFRENTAMIENTO CONTRA OTRO JUGADOR
        public function actionEnfrentamiento($id)
    {
        //LUCHADOR
        $model = $this->findModel($id);    
        $imgpath = $this->actionSelectclase($model->clase);
        
        $clase = Clases::findOne($model->nom_clase);
        $imgpathClass = $this->actionSelecttipo($model->nom_clase);
        
        $elemento = Elementos::findOne($model->nom_elemento);
        $imgpathElement =  $this->actionSelectelement($model->nom_elemento);
        
        $zona = Zonas::findOne($model->nom_zona);
        $imgpathZone = $this->actionSelectzona($model->nom_zona);


        //CONTRINCANTE
        $enemymodel = $this->findModel(Personajes::find()->select('cod')->where(['muerto' => 0])->andWhere("cod != $model->cod")->orderBy('rand()')->one());           
        if ($enemymodel == null)
        {//SI NO HAY CONTRINCANTES USA UN MUERTO
          $enemymodel = $this->findModel(Personajes::find()->select('cod')->where(['muerto' => 1])->andWhere("cod != $model->cod")->orderBy('rand()')->one());             
        }
        
        $enemyimgpath = $this->actionSelectclase($enemymodel->clase);
                
        $enemyclase = Clases::findOne($enemymodel->nom_clase);
        $enemyimgpathClass = $this->actionSelecttipo($enemymodel->nom_clase);
        
        $enemyelemento = Elementos::findOne($enemymodel->nom_elemento);
        $enemyimgpathElement = $this->actionSelectelement($enemymodel->nom_elemento);
        
        $enemyzona = Zonas::findOne($enemymodel->nom_zona);
       
        ///DATA PROVIDERS
        $dataProvider = new ActiveDataProvider([
            'query' => Personajes::find()->where(['cod' => $model->cod])
        ]);
        
        $enemydataProvider = new ActiveDataProvider([
            'query' => Personajes::find()->where(['cod' => $enemymodel->cod]),
        ]);
        
        //RENDER
        return $this->render('enfrentamiento', [
            'dataProvider' => $dataProvider,
            'enemydataProvider' => $enemydataProvider,         
            'model' => $model,
            'imgpath' => $imgpath,
            'clase' => $clase,
            'imgpathClass' => $imgpathClass,
            'elemento' => $elemento,
            'imgpathElement' => $imgpathElement,
            'zona' => $zona,
            'imgpathZone' => $imgpathZone,           
            'enemymodel' => $enemymodel,
            'enemyimgpath' => $enemyimgpath,
            'enemyclase' => $enemyclase,
            'enemyimgpathClass' => $enemyimgpathClass,
            'enemyelemento' => $enemyelemento,
            'enemyimgpathElement' => $enemyimgpathElement,
            'enemyzona' => $enemyzona,
        ]);
    }
    
    //MOSTRAR EL RESULTADO DEL COMBATE, ADAPTANDO LOS MODELOS DE AMBOS PERSONAJES
    public function actionResultadocombate($id1, $id2, $fuerza1, $fuerza2, $rendido)
    {
        $model1 = $this->findModel($id1);
        $model2 = $this->findModel($id2);
        
        $totalfuerza = $fuerza1+$fuerza2;
        $calcGanador = rand(1, $totalfuerza);
        $resultado = '';
        $destinoniveles = '';
        $destinojugador = '';
        $ganador = $model1;
        $perdedor = $model2;
        $imgclase = $this->actionSelectclase($model1->clase);
        $matanza = 0;
        
        //SI NO TE RINDES
        if (!$rendido)
        {
            //GANAS
            if ($calcGanador <= $fuerza1)
            {
                $resultado = 'Ganas el combate!';
                $ganador = $model1->cod;
                $perdedor = $model2->cod;
                
                //CALCULAR NIVELES A SUMAR/RESTAR
                if ($fuerza1 > $fuerza2)
                {
                    $model1->nivel += 1; $model1->save();
                    $destinoniveles = 'ganas 1 nivel!';
                    $model2->nivel -= 1; $model2->save();
                } else if ($fuerza1 == $fuerza2)
                {
                    $model1->nivel += 2; $model1->save();
                    $destinoniveles = 'ganas 2 niveles!';
                    $model2->nivel -= 2; $model2->save();
                } else if ($fuerza1 < $fuerza2)
                {
                    $model1->nivel += 3; $model1->save();
                    $destinoniveles = 'ganas 3 niveles!';
                    $model2->nivel -= 3; $model2->save();
                }
            }
            //PIERDES
            else
            {
                $resultado = 'Perdiste! :(';
                $ganador = $model2->cod;
                $perdedor = $model1->cod;
                //CALCULAR NIVELES A SUMAR/RESTAR
                if ($fuerza2 > $fuerza1)
                {
                    $model2->nivel += 1; $model2->save();
                    $model1->nivel -= 1; $model1->save();
                    $destinoniveles = 'pierdes 1 nivel!';
                } else if ($fuerza2 == $fuerza1)
                {
                    $model2->nivel += 2; $model2->save();
                    $model1->nivel -= 2; $model1->save();
                    $destinoniveles = 'pierdes 2 niveles!';
                } else if ($fuerza2 < $fuerza1)
                {
                    $model2->nivel += 3; $model2->save();
                    $model1->nivel -= 3; $model1->save();
                    $destinoniveles = 'pierdes 3 niveles!';
                }
            }
        } else
        {//DECIDES HUIR
            $resultado = "has decidido huir!";
            $calcGanador = rand(1, 12);
            //POSIBILIDAD DE PERDER NIVELES
            if ($calcGanador <= 1)
            {
               $model1->nivel -= 2; $model1->save();
               $destinoniveles = 'perdiste 2 niveles!';
            } else if ($calcGanador > 1 && $calcGanador <= 4)
            {
                $model1->nivel -= 1; $model1->save();
                $destinoniveles = 'perdiste 1 nivel!';
            } else
            {
                $destinoniveles = 'Escapastes sin problemas...';
            }
        }
        
        //MIRAR SI ALGUIEN HA MUERTO          
        if ($model1->nivel <= 0)
            {
                $model1->muerto = 1; $model1->nivel = 0; $model1->save();
                $destinojugador = 'tu personaje ha muerto!!!';
                $matanza = 1;
            }
        if ($model2->nivel <= 0)
            {
                $model2->muerto = 1; $model2->nivel = 0; $model2->save();
                $destinojugador = 'tu contrincante ha muerto!!!';
                $matanza = 1;
            }
       //GUARDAR LA PARTIDO JUGADA (PARTIDAS EN LAS QUE HUYES NO SE REGISTRAN)     
       if (!$rendido)   
       {
        $modelPartidas = new Partidas();

        $modelPartidas->cod_jugador1 = $model1->cod;
        $modelPartidas->cod_jugador2 = $model2->cod;
        $modelPartidas->ganador = $ganador;
        $modelPartidas->perdedor = $perdedor;
        $modelPartidas->matanza = $matanza;
        $modelPartidas->save(false);
       }
        $dataProvider = new ActiveDataProvider([
            'query' => Personajes::find()->where(['cod' => $model1->cod])
        ]);
        
        $enemydataProvider = new ActiveDataProvider([
            'query' => Personajes::find()->where(['cod' => $model2->cod]),
        ]);
      
     //SI ESTAS VIVO MOSTRAR RESULTADO COMBATE   
     if ($model1->nivel > 0)  
     {
        return $this->render('resultadocombate', [
            'dataProvider' => $dataProvider,
            'enemydataProvider' => $enemydataProvider,
            'model1' => $model1,
            'model2' => $model2,
            'fuerza1' => $fuerza1,
            'fuerza2' => $fuerza2,
            'resultado' => $resultado,
            'destinojugador' => $destinojugador,
            'destinoniveles' => $destinoniveles,
            'imgclase' => $imgclase,
        ]);
     }
     //SI ESTAS MUERTO MOSTRAR RESULTADO COMBATE SIN OPCION DE VOLVER A LUCHAR
     if ($model1->nivel <= 0)  
     {
        return $this->render('resultadocombatelose', [
            'dataProvider' => $dataProvider,
            'enemydataProvider' => $enemydataProvider,
            'model1' => $model1,
            'model2' => $model2,
            'fuerza1' => $fuerza1,
            'fuerza2' => $fuerza2,
            'resultado' => $resultado,
            'destinojugador' => $destinojugador,
            'destinoniveles' => $destinoniveles,
            'imgclase' => $imgclase,
        ]);
     }
        
    }
    
    public function actionCrearpersonaje()
    {
        $model = new Personajes();
        
        $model_clases = Clases::find()->all();
        $model_elementos = Elementos::find()->all();
        $model_zonas = Zonas::find()->all();
        

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
      //ELEGIR CLASE DEPENDIENDO DE TU TIPO Y DE TU ELEMENTO
      switch ($model->nom_elemento) 
        {
      case 'Fuego': //CLASES DE FUEGO
          switch ($model->nom_clase) 
        {
          case 'Guerrero':
          $model->setAttribute('clase', 'Herrero');
              break;
          case 'Arquero':
          $model->setAttribute('clase', 'Caza-Dragones');
              break;
          case 'Mago':
          $model->setAttribute('clase', 'Incendiario');
              break;
        }
        break;
          case 'Agua': //CLASES DE AGUA
            switch ($model->nom_clase) 
        {
                case 'Guerrero':
          $model->setAttribute('clase', 'Pirata');
              break;
          case 'Arquero':
          $model->setAttribute('clase', 'Avalanchero');
              break;
          case 'Mago':
          $model->setAttribute('clase', 'Aguacante');
              break;
        }
        break;

                case 'Tierra': //CLASES DE TIERRA
            switch ($model->nom_clase) 
        {
          case 'Guerrero':
          $model->setAttribute('clase', 'Mercenario');
              break;
          case 'Arquero':
          $model->setAttribute('clase', 'Cazador');
              break;
          case 'Mago':
          $model->setAttribute('clase', 'Djinn');
              break;
        }
        break;
          case 'Viento': //CLASES DE VIENTO
            switch ($model->nom_clase) 
        {
          case 'Guerrero':
          $model->setAttribute('clase', 'Caballuco');
              break;
          case 'Arquero':
          $model->setAttribute('clase', 'Ronin');
              break;
          case 'Mago':
          $model->setAttribute('clase', 'Tormentador');
              break;
        }
        break;
        }
        $model->save();
            return $this->redirect(['personajesingular', 'id' => $model->cod]);
        }

        return $this->render('crearpersonaje', [
            'model' => $model,
            'model_clases' => $model_clases,
            'model_elementos' => $model_elementos,
            'model_zonas' => $model_zonas,
        ]);
    }
    
    //ELEGIR UN TIPO
    public function actionSelecttipo($id)
    {
        if ($id == 'Arquero')
        {
            return "/imgs/weapons/class/Arquero.png";
        }
        if ($id == 'Guerrero')
        {
            return "/imgs/weapons/class/Guerrero.png";
        }
        if ($id == 'Mago')
        {
            return "/imgs/weapons/class/Mago.png";
        }
    }
    //ELEGIR UN TIPO FUERTE
    public function actionSelecttipofuerte($id)
    {
        if ($id == 'Arquero')
        {
            return "/imgs/weapons/class/Mago.png";
        }
        if ($id == 'Guerrero')
        {
            return "/imgs/weapons/class/Arquero.png";
        }
        if ($id == 'Mago')
        {
            return "/imgs/weapons/class/Guerrero.png";
        }
    }
    //ELEGIR UN TIPO DEBIL
    public function actionSelecttipodebil($id)
    {
        if ($id == 'Arquero')
        {
            return "/imgs/weapons/class/Guerrero.png";
        }
        if ($id == 'Guerrero')
        {
            return "/imgs/weapons/class/Mago.png";
        }
        if ($id == 'Mago')
        {
            return "/imgs/weapons/class/Arquero.png";
        }
    }
    //ELEGIR UN ELEMENTO
    public function actionSelectelement($id)
    {
        if ($id == 'Fuego')
        {
            return "/imgs/weapons/element/Fuego.png";
        }
        if ($id == 'Agua')
        {
            return "/imgs/weapons/element/Agua.png";
        }
        if ($id == 'Tierra')
        {
            return "/imgs/weapons/element/Tierra.png";
        }
         if ($id == 'Viento')
        {
            return "/imgs/weapons/element/Viento.png";
        }
    }
    //ELEGIR UN ELEMENTOFUERTE
    public function actionSelectelementfuerte($id)
    {
        if ($id == 'Fuego')
        {
            return "/imgs/weapons/element/Tierra.png";
        }
        if ($id == 'Agua')
        {
            return "/imgs/weapons/element/Fuego.png";
        }
        if ($id == 'Tierra')
        {
            return "/imgs/weapons/element/Viento.png";
        }
         if ($id == 'Viento')
        {
            return "/imgs/weapons/element/Agua.png";
        }
    }
    //ELEGIR UN ELEMENTO DEBIL
    public function actionSelectelementdebil($id)
    {
        if ($id == 'Fuego')
        {
            return "/imgs/weapons/element/Agua.png";
        }
        if ($id == 'Agua')
        {
            return "/imgs/weapons/element/Viento.png";
        }
        if ($id == 'Tierra')
        {
            return "/imgs/weapons/element/Fuego.png";
        }
         if ($id == 'Viento')
        {
            return "/imgs/weapons/element/Tierra.png";
        }
    }
    //ELEGIR UNA IMAGEN DE CLASE
    public function actionSelectclase($id)
    {
        switch ($id)
        {
        case ('Herrero'):
            $imgpath = "/imgs/chars/Guerrero/Fuego.png";
            break;
        case ('Caza-Dragones'):
            $imgpath = "/imgs/chars/Arquero/Fuego.jpg";
            break;
        case ('Incendiario'):
            $imgpath = "/imgs/chars/Mago/Fuego.jpg";
            break;
        case ('Pirata'):
            $imgpath = "/imgs/chars/Guerrero/Agua.jpg";
            break;
        case ('Avalanchero'):
            $imgpath = "/imgs/chars/Arquero/Agua.jpg";
            break;
        case ('Aguacante'):
            $imgpath = "/imgs/chars/Mago/Agua.jpg";
            break;
        case ('Mercenario'):
            $imgpath = "/imgs/chars/Guerrero/Tierra.png";
            break;
        case ('Cazador'):
            $imgpath = "/imgs/chars/Arquero/Tierra.png";
            break;
        case ('Djinn'):
            $imgpath = "/imgs/chars/Mago/Tierra.jpg";
            break;
        case ('Caballuco'):
            $imgpath = "/imgs/chars/Guerrero/Viento.png";
            break;
        case ('Ronin'):
            $imgpath = "/imgs/chars/Arquero/Viento.png";
            break;
        case ('Tormentador'):
            $imgpath = "/imgs/chars/Mago/Viento.jpg";
            break;
        }
        return $imgpath;
    }
    //ELEGIR UNA ZONA
    public function actionSelectzona($id)
    {    
        switch ($id)
        {
            case ("Desierto"):
                $imgpath = "/imgs/zones/wasteland.png";
                break;
            case ("Montaña"):
                $imgpath = "/imgs/zones/mountain.jpg";
                break;
            case ("Cueva"):
                $imgpath = "/imgs/zones/cave.jpg";
                break;
            case ("Bosque"):
                $imgpath = "/imgs/zones/forest.png";
                break;
        }
        return $imgpath;
    }
    //ELEGIR UNA ZONA DEBIL
    public function actionSelectzonadebil($id)
    {    
        switch ($id)
        {
            case ("Desierto"):
                
                $imgpath = "/imgs/zones/cave.jpg";
                break;
            case ("Montaña"):
                $imgpath = "/imgs/zones/wasteland.png";
                break;
            case ("Cueva"):
                $imgpath = "/imgs/zones/forest.png";
                break;
            case ("Bosque"):
                $imgpath = "/imgs/zones/mountain.jpg";
                break;
        }
        return $imgpath;
    }
}